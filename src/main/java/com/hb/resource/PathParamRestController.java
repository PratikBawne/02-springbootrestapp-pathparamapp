package com.hb.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PathParamRestController {

	private String rest_endpoint_url = "www.google.com";

	@GetMapping("/course/{cname}")
	public String gertCourseDetails(@PathVariable("cname") String cname) {
		if("JRTP".equals(cname)) {
			return "JRTP Batch is starting from 05-Apr-2021";
		}
		else if("SBMS".equals(cname)) {
			return "SBMS Batch is starting from 10-Apr-2021";
		}
		else {
			return "Please check our website at www.ashokit.in";
		}
	}
	
	@GetMapping("/course/{cname}/{fname}")
	public String gertCourseDetails(@PathVariable("cname") String cname, @PathVariable("fname") String fname) {
		if("JRTP".equals(cname) && "ASHOK".equals(fname)) {
			return "JRTP Batch Duration is 3 months";
		}
		else if("SBMS".equals(cname) && "ASHOK".equals(fname)) {
			return "SBMS Batch Duration is 4 months";
		}
		else {
			return "Please check our website at www.ashokit.in";
		}
	}
	
	@GetMapping("/course/{cname}/fastrack/{tname}")
	public String getDetailsOfCourse(@PathVariable("cname") String cname, @PathVariable("tname") String tname) {
		return "Registration Process Is Started";
	}
	
}
